# Bath Air Quality

A Thingful fetcher for getting data from the almost real time air monitoring stations in Bath, UK.

This project is licensed under the terms of the MIT license.