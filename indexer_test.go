package bathaq

import (
	"context"
	"io/ioutil"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/thingful/httpmock"
	"github.com/thingful/thingfulx"
	"github.com/thingful/thingfulx/schema"
)

func TestNewFetcher(t *testing.T) {
	_, err := NewIndexer()
	assert.Nil(t, err)
}

func TestURLS(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	jsonData, err := ioutil.ReadFile("data/valid-allstations.json")

	assert.Nil(t, err)

	httpmock.Reset()
	httpmock.RegisterStubRequest(
		httpmock.NewStubRequest(
			"GET",
			"https://data.bathhacked.org/resource/fjzm-5zsg.json",
			httpmock.NewBytesResponder(http.StatusOK, jsonData),
		),
	)

	expected := []string{
		"https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
		"https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=London+Road+Enclosure",
		"https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=London+Road+AURN",
		"https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Royal+Victoria+Park",
		"https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Windsor+Bridge",
	}

	delay := time.Duration(0)

	got, err := indexer.URLS(context.Background(), client, delay)
	assert.Nil(t, err)

	assert.Equal(t, expected, got)
}

func TestFetchValid(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	rawData1, err := ioutil.ReadFile("data/valid-station.json")
	assert.Nil(t, err)

	rawData2, err := ioutil.ReadFile("data/valid-station2.json")
	assert.Nil(t, err)

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       []byte
	}{
		{
			url:            "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
			respStatusCode: http.StatusOK,
			respBody:       rawData1,
		},
		{
			url:            "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
			respStatusCode: http.StatusOK,
			respBody:       rawData2,
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewBytesResponder(testcase.respStatusCode, testcase.respBody),
			),
		)

		ctx := context.Background()

		_, err := indexer.Fetch(ctx, testcase.url, client)
		assert.Nil(t, err)
	}
}

func TestParseValid(t *testing.T) {

	indexer, _ := NewIndexer()

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	testRecordedAt, _ := time.Parse("2006-01-02T15:04:05", "2016-02-12T13:45:00")

	rawData1, err := ioutil.ReadFile("data/valid-station.json")
	assert.Nil(t, err)

	rawData2, err := ioutil.ReadFile("data/valid-station2.json")
	assert.Nil(t, err)

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       []byte
		expected       []thingfulx.Thing
	}{
		{
			url:            "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
			respStatusCode: http.StatusOK,
			respBody:       rawData1,
			expected: []thingfulx.Thing{
				{
					Title:       "Bath Guildhall air quality sensor",
					Description: "",
					Category:    thingfulx.Environment,
					Webpage:     "https://data.bathhacked.org/Environment/-Live-Air-Quality-Sensor-Data/hqr9-djir/#guildhall",
					IndexedAt:   timeProvider.Now(),

					Location: &thingfulx.Location{

						Lat: 51.3822189160466,
						Lng: -2.35903402755172,
					},
					Provider: &thingfulx.Provider{
						Name:        provider,
						ID:          indexerUID,
						URL:         webpage,
						Description: "Air quality data from sensors across B&NES. Measurements collected at 15-60 min intervals.",
					},
					Visibility: thingfulx.Open,
					Endpoint: &thingfulx.Endpoint{
						URL:         "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
						ContentType: "application/json",
					},
					Metadata:        []thingfulx.Metadata{},
					ThingType:       schema.Expand("thingful:ConnectedDevice"),
					DataLicense:     thingfulx.GetDataLicense(dataLicenseURL),
					AttributionName: attributionName,
					AttributionURL:  attributionURL,
					Channels: []thingfulx.Channel{
						{
							ID:               "co",
							Metadata:         []thingfulx.Metadata{},
							Type:             schema.DoubleType,
							QuantityKind:     schema.Expand("m3-lite:ChemicalAgentAtmosphericConcentrationCO"),
							MeasuredBy:       schema.Expand("m3-lite:AirPollutantSensor"),
							DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
							Unit:             schema.Expand("m3-lite:PPM"),
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecordedAt,
									Location: &thingfulx.Location{
										Lat: 51.3822189160466,
										Lng: -2.35903402755172,
									},
									Val: "999.0000",
								},
							},
						},
						{
							ID:               "nox",
							Metadata:         []thingfulx.Metadata{},
							Type:             schema.DoubleType,
							QuantityKind:     schema.Expand("thingfulqu:ChemicalAgentAtmosphericConcentrationNOx"),
							MeasuredBy:       schema.Expand("m3-lite:NOSensor"),
							DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
							Unit:             schema.Expand("thingfulqu:PPB"),
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecordedAt,
									Location: &thingfulx.Location{
										Lat: 51.3822189160466,
										Lng: -2.35903402755172,
									},
									Val: "84.7988",
								},
							},
						},
						{
							ID:               "no2",
							Metadata:         []thingfulx.Metadata{},
							Type:             schema.DoubleType,
							QuantityKind:     schema.Expand("m3-lite:ChemicalAgentAtmosphericConcentrationNO2"),
							MeasuredBy:       schema.Expand("m3-lite:NOSensor"),
							DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
							Unit:             schema.Expand("thingfulqu:PPB"),
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecordedAt,
									Location: &thingfulx.Location{
										Lat: 51.3822189160466,
										Lng: -2.35903402755172,
									},
									Val: "31.0996",
								},
							},
						},
						{
							ID:               "no",
							Metadata:         []thingfulx.Metadata{},
							Type:             schema.DoubleType,
							QuantityKind:     schema.Expand("thingfulqu:ChemicalAgentAtmosphericConcentrationNO"),
							MeasuredBy:       schema.Expand("m3-lite:NOSensor"),
							DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
							Unit:             schema.Expand("thingfulqu:PPB"),
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecordedAt,
									Location: &thingfulx.Location{
										Lat: 51.3822189160466,
										Lng: -2.35903402755172,
									},
									Val: "53.6992",
								},
							},
						},
					},
				},
			},
		},
		{
			url:            "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
			respStatusCode: http.StatusOK,
			respBody:       rawData2,
			expected: []thingfulx.Thing{
				{
					Title:       "Bath Guildhall air quality sensor",
					Description: "",
					Category:    thingfulx.Environment,
					Webpage:     "https://data.bathhacked.org/Environment/-Live-Air-Quality-Sensor-Data/hqr9-djir/#guildhall",
					IndexedAt:   timeProvider.Now(),
					Location: &thingfulx.Location{
						Lat: 51.3822189160466,
						Lng: -2.35903402755172,
					},
					Provider: &thingfulx.Provider{
						Name:        provider,
						ID:          indexerUID,
						URL:         webpage,
						Description: "Air quality data from sensors across B&NES. Measurements collected at 15-60 min intervals.",
					},
					Visibility: thingfulx.Open,
					Endpoint: &thingfulx.Endpoint{
						URL:         "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
						ContentType: "application/json",
					},
					Metadata:        []thingfulx.Metadata{},
					ThingType:       schema.Expand("thingful:ConnectedDevice"),
					DataLicense:     thingfulx.GetDataLicense(dataLicenseURL),
					AttributionName: attributionName,
					AttributionURL:  attributionURL,
					Channels: []thingfulx.Channel{
						{
							ID:               "co",
							Metadata:         []thingfulx.Metadata{},
							Type:             schema.DoubleType,
							QuantityKind:     schema.Expand("m3-lite:ChemicalAgentAtmosphericConcentrationCO"),
							MeasuredBy:       schema.Expand("m3-lite:AirPollutantSensor"),
							DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
							Unit:             schema.Expand("m3-lite:PPM"),
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecordedAt,
									Location: &thingfulx.Location{
										Lat: 51.3822189160466,
										Lng: -2.35903402755172,
									},
									Val: "999.0000",
								},
							},
						},
						{
							ID:               "nox",
							Metadata:         []thingfulx.Metadata{},
							Type:             schema.DoubleType,
							QuantityKind:     schema.Expand("thingfulqu:ChemicalAgentAtmosphericConcentrationNOx"),
							MeasuredBy:       schema.Expand("m3-lite:NOSensor"),
							DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
							Unit:             schema.Expand("thingfulqu:PPB"),
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecordedAt,
									Location: &thingfulx.Location{
										Lat: 51.3822189160466,
										Lng: -2.35903402755172,
									},
									Val: "84.7988",
								},
							},
						},
						{
							ID:               "no",
							Metadata:         []thingfulx.Metadata{},
							Type:             schema.DoubleType,
							QuantityKind:     schema.Expand("thingfulqu:ChemicalAgentAtmosphericConcentrationNO"),
							MeasuredBy:       schema.Expand("m3-lite:NOSensor"),
							DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
							Unit:             schema.Expand("thingfulqu:PPB"),
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecordedAt,
									Location: &thingfulx.Location{
										Lat: 51.3822189160466,
										Lng: -2.35903402755172,
									},
									Val: "53.6992",
								},
							},
						},
						{
							ID:               "pm10",
							Metadata:         []thingfulx.Metadata{},
							Type:             schema.DoubleType,
							QuantityKind:     schema.Expand("thingfulqu:AtmosphericConcentrationAirParticlesPM10"),
							MeasuredBy:       schema.Expand("m3-lite:AirPollutantSensor"),
							DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
							Unit:             schema.Expand("m3-lite:MicrogramPerCubicMetre"),
							Observations: []thingfulx.Observation{
								{
									RecordedAt: testRecordedAt,
									Location: &thingfulx.Location{
										Lat: 51.3822189160466,
										Lng: -2.35903402755172,
									},
									Val: "10,000,000,000",
								},
							},
						},
					},
				},
			},
		},
	}

	for _, testcase := range testcases {

		things, err := indexer.Parse([]byte(testcase.respBody), testcase.url, timeProvider)
		assert.Nil(t, err)

		assert.Equal(t, testcase.expected, things)
	}

}

func TestFetchInvalid(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	validRawData, err := ioutil.ReadFile("data/valid-station.json")
	assert.Nil(t, err)

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       []byte
	}{
		{
			url:            "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
			respStatusCode: http.StatusNotFound,
			respBody:       validRawData,
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewBytesResponder(testcase.respStatusCode, testcase.respBody),
			),
		)

		ctx := context.Background()

		_, err := indexer.Fetch(ctx, testcase.url, client)

		assert.NotNil(t, err)

	}
}

func TestParseInvalid(t *testing.T) {

	indexer, _ := NewIndexer()

	timeProvider := thingfulx.NewMockTimeProvider(time.Now())

	invalidRawData, err := ioutil.ReadFile("data/invalid-station.json")
	assert.Nil(t, err)

	testcases := []struct {
		url            string
		respStatusCode int
		respBody       []byte
	}{
		{
			url:            "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
			respStatusCode: http.StatusOK,
			respBody:       invalidRawData,
		},
	}

	for _, testcase := range testcases {

		assert.Nil(t, err)

		_, err = indexer.Parse([]byte(testcase.respBody), testcase.url, timeProvider)

		assert.NotNil(t, err)
	}

}

func TestFetchResponseError(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []struct {
		url            string
		respStatusCode int
		expected       error
	}{
		{
			url:            "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
			respStatusCode: http.StatusNotFound,
			expected:       thingfulx.ErrNotFound,
		},
		{
			url:            "https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
			respStatusCode: http.StatusRequestTimeout,
			expected:       thingfulx.NewErrUnexpectedResponse("408"),
		},
	}

	for _, testcase := range testcases {
		httpmock.Reset()
		httpmock.RegisterStubRequest(
			httpmock.NewStubRequest(
				"GET",
				testcase.url,
				httpmock.NewBytesResponder(testcase.respStatusCode, []byte{}),
			),
		)

		ctx := context.Background()

		_, err := indexer.Fetch(ctx, testcase.url, client)
		assert.NotNil(t, err)

		assert.Equal(t, err, testcase.expected)
	}
}

func TestFetchError(t *testing.T) {
	indexer, _ := NewIndexer()

	client := thingfulx.NewClient("thingful", time.Duration(1)*time.Second)

	testcases := []string{"", "%"}

	for _, url := range testcases {
		ctx := context.Background()

		_, err := indexer.Fetch(ctx, url, client)
		assert.NotNil(t, err)
	}
}
