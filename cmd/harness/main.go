// +build harness

package main

import (
	"time"

	"bitbucket.org/thingful/bathaq" //change template to package name
	"github.com/thingful/testharness"
	"golang.org/x/net/context"
)

var URLs []string
var err error

func main() {

	harness, err := testharness.Register(bathaq.NewIndexer, true) //change template to package name

	if err != nil {
		panic(err)
	}

	// run everything, Provider, URLs and Fetch
	fetchAllInterval := time.Duration(5) * time.Second // interval between each Fetch
	urlsToFetch := 10                                  // total of urls to Fetch
	harness.RunAll(context.Background(), fetchAllInterval, urlsToFetch)

	// or you can  fetch specific urls like this
	// urls := []string{"https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=Bath+Guildhall",
	// 	"https://data.bathhacked.org/resource/hqr9-djir.json?%24limit=1&%24order=datetime+DESC&sensor_location_name=London+Road+AURN",
	// }

	// harness.RunFetch(urls)

}
