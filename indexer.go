// Package bathaq is a fetcher implementation that collects
// air quality data from monitoring stations Bath, UK.
package bathaq

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"time"

	"github.com/thingful/thingfulx"
	"github.com/thingful/thingfulx/schema"
)

const (
	// indexerUID is the name of this fetcher
	indexerUID = "bathaq"

	// provider the data provider name
	provider = "Bath: Hacked - Air Quality"

	// dataLicenseURL is the url of te datalicense
	dataLicenseURL = thingfulx.OGLV2URL

	// attributionName is the name of copyright holder and/or author of this data
	attributionName = "Bath & North East Somerset Council"

	// attributionURL is the url of copyright holder and/or author of this data
	attributionURL = "http://www.bathnes.gov.uk/"

	// webpage the upstream provider
	webpage = "http://www.bathhacked.org/"

	// providerURL the upstream data html webpage
	providerURL = "https://data.bathhacked.org/Environment/-Live-Air-Quality-Sensor-Data/hqr9-djir"

	// dataURL is the API endpoint
	dataURL = "https://data.bathhacked.org/resource/hqr9-djir.json"

	// stationsURL is the endpoint used to get the availbale stations
	stationsURL = "https://data.bathhacked.org/resource/fjzm-5zsg.json"
)

// NewIndexer instantiates a new Indexer instance. All fetchers must try to
// load any required config values from namespaced environment variables.
func NewIndexer() (thingfulx.Indexer, error) {

	provider := &thingfulx.Provider{
		Name:        provider,
		ID:          indexerUID,
		URL:         webpage,
		Description: "Air quality data from sensors across B&NES. Measurements collected at 15-60 min intervals.",
	}

	return &indexer{
		provider: provider,
	}, nil
}

// indexer is our indexer type
type indexer struct {
	provider *thingfulx.Provider
}

// Stations struct can parse stations data and store their names
type Stations struct {
	Name string `json:"title"`
}

// Station struct can parse an individual station data object
type Station struct {
	Location     location `json:"sensor_location"`
	Nox          string   `json:"nox"`
	No2          string   `json:"no2"`
	No           string   `json:"no"`
	Pm10         string   `json:"pm10"`
	Co           string   `json:"co"`
	LocationName string   `json:"sensor_location_name"`
	Datetime     string   `json:"datetime"`
	Slug         string   `json:"sensor_location_slug"`
}

type location struct {
	Longitude string `json:"longitude"`
	Latitude  string `json:"latitude"`
}

// UID return unique identifier of this fetcher
func (i *indexer) UID() string {
	return indexerUID
}

// URLS returns the minimum set of urls required to fully index this data provider
func (i *indexer) URLS(ctx context.Context, client thingfulx.Client, delay time.Duration) ([]string, error) {
	var stations []Stations
	var URLs []string

	req, err := http.NewRequest("GET", stationsURL, nil)
	if err != nil {
		return nil, err
	}

	resp, err := client.DoHTTP(req)
	if err != nil {
		return nil, err
	}

	defer func() {
		if cerr := resp.Body.Close(); err == nil && cerr != nil {
			err = cerr
		}
	}()

	// parse data and augment dataURL
	err = json.NewDecoder(resp.Body).Decode(&stations)
	if err != nil {
		return nil, err
	}

	for _, station := range stations {
		u, _ := url.Parse(dataURL)
		v := url.Values{}

		v.Set("$limit", "1")             // only one result
		v.Add("$order", "datetime DESC") // most recent value
		v.Add("sensor_location_name", station.Name)

		u.RawQuery = v.Encode()

		URLs = append(URLs, u.String())
	}

	return URLs, nil

}

// Fetch a resource from the upstream provider if we can.
func (i *indexer) Fetch(ctx context.Context, url string, client thingfulx.Client) ([]byte, error) {

	bytes, err := client.DoHTTPGetRequest(url)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

// Parse is our function to actually extract data and return the slice of things
func (i *indexer) Parse(rawData []byte, urlString string, timeprovider thingfulx.TimeProvider) ([]thingfulx.Thing, error) {
	things := []thingfulx.Thing{}
	var sData []Station

	// decode data
	err := json.NewDecoder(bytes.NewReader(rawData)).Decode(&sData)
	if err != nil {
		return nil, err
	}

	for _, s := range sData {

		// parse date
		recordedAt, err := time.Parse("2006-01-02T15:04:05", s.Datetime)
		if err != nil {
			return nil, err
		}

		// convert longitude to float64
		lon, err := strconv.ParseFloat(s.Location.Longitude, 64)
		if err != nil {
			return nil, err
		}

		// convert latitude to float64
		lat, err := strconv.ParseFloat(s.Location.Latitude, 64)
		if err != nil {
			return nil, err
		}

		thing := thingfulx.Thing{
			Title:       fmt.Sprintf("%s air quality sensor", s.LocationName),
			Description: "",
			Category:    thingfulx.Environment,
			Webpage:     fmt.Sprintf("%s/#%s", providerURL, s.Slug),
			IndexedAt:   timeprovider.Now(),
			Location: &thingfulx.Location{
				Lat: lat,
				Lng: lon,
			},
			Provider:   i.provider,
			Visibility: thingfulx.Open,
			Endpoint: &thingfulx.Endpoint{
				URL:         urlString,
				ContentType: "application/json",
			},
			Metadata:        []thingfulx.Metadata{},
			ThingType:       schema.Expand("thingful:ConnectedDevice"),
			DataLicense:     thingfulx.GetDataLicense(dataLicenseURL), // need to check this
			AttributionName: attributionName,
			AttributionURL:  attributionURL,
		}

		// add channels
		thing.Channels = buildChannel(s, recordedAt, thing.Location)

		things = append(things, thing)
	}

	return things, nil
}

func buildChannel(data Station, recordedAt time.Time, loc *thingfulx.Location) []thingfulx.Channel {
	var channels []thingfulx.Channel

	// do not include empty channels
	if data.Co != "" {
		t := thingfulx.Channel{
			ID:               "co",
			Metadata:         []thingfulx.Metadata{},
			Type:             schema.DoubleType,
			QuantityKind:     schema.Expand("m3-lite:ChemicalAgentAtmosphericConcentrationCO"),
			MeasuredBy:       schema.Expand("m3-lite:AirPollutantSensor"),
			DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
			Unit:             schema.Expand("m3-lite:PPM"),
			Observations: []thingfulx.Observation{
				{
					RecordedAt: recordedAt,
					Location:   loc,
					Val:        data.Co,
				},
			},
		}

		channels = append(channels, t)
	}

	if data.Nox != "" {
		t := thingfulx.Channel{
			ID:               "nox",
			Metadata:         []thingfulx.Metadata{},
			Type:             schema.DoubleType,
			QuantityKind:     schema.Expand("thingfulqu:ChemicalAgentAtmosphericConcentrationNOx"),
			MeasuredBy:       schema.Expand("m3-lite:NOSensor"),
			DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
			Unit:             schema.Expand("thingfulqu:PPB"),
			Observations: []thingfulx.Observation{
				{
					RecordedAt: recordedAt,
					Location:   loc,
					Val:        data.Nox,
				},
			},
		}

		channels = append(channels, t)
	}

	if data.No2 != "" {
		t := thingfulx.Channel{
			ID:               "no2",
			Metadata:         []thingfulx.Metadata{},
			Type:             schema.DoubleType,
			QuantityKind:     schema.Expand("m3-lite:ChemicalAgentAtmosphericConcentrationNO2"),
			MeasuredBy:       schema.Expand("m3-lite:NOSensor"),
			DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
			Unit:             schema.Expand("thingfulqu:PPB"),
			Observations: []thingfulx.Observation{
				{
					RecordedAt: recordedAt,
					Location:   loc,
					Val:        data.No2,
				},
			},
		}

		channels = append(channels, t)
	}

	if data.No != "" {
		t := thingfulx.Channel{
			ID:               "no",
			Metadata:         []thingfulx.Metadata{},
			Type:             schema.DoubleType,
			QuantityKind:     schema.Expand("thingfulqu:ChemicalAgentAtmosphericConcentrationNO"),
			MeasuredBy:       schema.Expand("m3-lite:NOSensor"),
			DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
			Unit:             schema.Expand("thingfulqu:PPB"),
			Observations: []thingfulx.Observation{
				{
					RecordedAt: recordedAt,
					Location:   loc,
					Val:        data.No,
				},
			},
		}

		channels = append(channels, t)
	}

	if data.Pm10 != "" {
		t := thingfulx.Channel{
			ID:               "pm10",
			Metadata:         []thingfulx.Metadata{},
			Type:             schema.DoubleType,
			QuantityKind:     schema.Expand("thingfulqu:AtmosphericConcentrationAirParticlesPM10"),
			MeasuredBy:       schema.Expand("m3-lite:AirPollutantSensor"),
			DomainOfInterest: []string{schema.Expand("m3-lite:Environment")},
			Unit:             schema.Expand("m3-lite:MicrogramPerCubicMetre"),
			Observations: []thingfulx.Observation{
				{
					RecordedAt: recordedAt,
					Location:   loc,
					Val:        data.Pm10,
				},
			},
		}

		channels = append(channels, t)
	}

	return channels
}
